import time
import random

print("Hacking NASA...")
time.sleep(1)
print("20%")
time.sleep(1)
print("40%")
time.sleep(1)
print("60%")
time.sleep(1)
print("80%")
time.sleep(1)
while True:
    if random.randint(1,20) != 10:
        time.sleep(0.25)
        print("99%")
    else:
        break
print("100%")
print("Successfully hacked NASA!")