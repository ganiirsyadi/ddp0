kata = "Hello World"

# String indexing

print(kata[2])
print(kata[-1])
# print(kata[11])
# print(kata[-12])


# String slicing 

kata = "Hello World"

print(kata[0:5:1])  # [start:finish:steps] [0:indexTerakhir+1:1]
print(kata[:5])
print(kata[6:11:2])  # 6,8,10

print(kata[2:])
print(kata[-2:2:-1])
print(kata[::-1])
print(kata[4:-7])
print(kata[:])

# print(kata[0:6:2])
# print(kata[0:6])
# print(kata[0:])


# # Membalik String
# print(kata[::-1])