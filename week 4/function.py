def penambahan_1(x,y):
    print(x+y)

def penambahan_2(x,y):
    return x+y

def ambil_huruf_vokal(kata):
    lst_result = set()
    lst_vowel = ['a', 'i', 'u', 'e', 'o']
    for huruf in kata:
        if huruf in lst_vowel:
            lst_result.add(huruf)
    return list(lst_result)

def ambil_huruf_vokal_2(kata):
    lst_result = []
    lst_vowel = ['a', 'i', 'u', 'e', 'o']
    for huruf in kata:
        if huruf in lst_vowel:
            lst_result.append(huruf)
    lst_result_unique = []
    for huruf in lst_result:
        if huruf not in lst_result_unique:
            lst_result_unique.append(huruf)
    return lst_result_unique

penambahan_1(2,3)
penambahan_2(1,2)

hasil = penambahan_1(3,4)

print(hasil)

print(ambil_huruf_vokal("indonesiaaaaaaa"))

def is_leap(year):
    if year % 400 == 0:
        return True
    elif year % 4 == 0:
        if year % 100 == 0:
            return False
        else:
            return True

print(is_leap(1900))
print(is_leap(2000))
print(is_leap(1996))

def is_leap(year):
    if year % 400 == 0:
        return True
    if year % 100 == 0:
        return False
    if year % 4 == 0:
        return True
    return False

print(is_leap(1900))
print(is_leap(2000))
print(is_leap(1996))

str_a = "halo halo bandung. ibu kota pariangan. sudah lama beta."

def return_words_1(paragraph):
    lst_kalimat = paragraph.replace(".", "")
    return lst_kalimat.split(" ")

def return_words_2(paragraph):
    lst_kalimat = paragraph.split(".")
    lst_kalimat_clean = []
    for kalimat in lst_kalimat:
        lst_kalimat_clean.append(kalimat.strip())
    lst_kata = []
    for kalimat in lst_kalimat_clean:
        lst_kata.extend(kalimat.split(" "))
    lst_kata.remove('')
    return lst_kata

print(return_words_1(str_a))
print(return_words_2(str_a))